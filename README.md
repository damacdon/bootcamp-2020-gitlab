# v1 PreWork Finished Code

This package is intended as a what the students should have when entering into the bootcamp
on the first day.

# Compilation

In either case, the user should start by pulling the ATLAS `AnalysisBase,21.2.125` docker image
and entering into the docker image as below
```
docker run --rm -it -v $PWD:/home/atlas/Bootcamp atlas/analysisbase:21.2.125 bash
```
which will place the entire file structure within the `/home/atlas/Bootcamp` directory
of the image.  Go one level above the directory of the repository and create a build directory.
In this directory, perform the CMake configuration and compilation
```
cd ..
mkdir build
cd build
cmake ../v1-prework-finished-code/
make
```
which will produce a locally executable `AnalysisPayload` again.

# Output

This code should produce a pair of histograms that shows the number of jets and the
dijet invariant mass of the two leading jets among all of the jets in each event.

